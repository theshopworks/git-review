<?php

namespace Shopworks\Tests\Unit\Commit;

use Mockery;
use Mockery\MockInterface;
use Shopworks\Git\Review\Commit\Author;
use Shopworks\Git\Review\Commit\Commit;
use Shopworks\Git\Review\Commit\CommitParser;
use Shopworks\Git\Review\Commit\Message;
use Shopworks\Git\Review\File\File;
use Shopworks\Git\Review\File\FileCollection;
use Shopworks\Git\Review\Process\Process as GitReviewProcess;
use Shopworks\Git\Review\Process\Processor;
use Shopworks\Tests\UnitTestCase;

class CommitParserTest extends UnitTestCase
{
    /** @var MockInterface|Processor $processor */
    private $processor;
    /** @var CommitParser $processor */
    private $sut;
    /** @var MockInterface|GitReviewProcess $process */
    private $process;

    public function setUp(): void
    {
        $this->sut = new CommitParser(
            $this->processor = Mockery::mock(Processor::class),
            $this->process = Mockery::mock(GitReviewProcess::class),
            \getcwd()
        );
    }

    /** @test */
    public function it_can_parse_a_git_log_and_separate_into_individual_commit_objects(): void
    {
                $commit = <<<'EOT'
Hash: a6e77b8f6686a6b97cd5cf27ad1cc3f6894edc0d
Parents: 4385b4ee83c6afb85434eb9aab36f94a347738b5
Subject: Commit subject a
Author Name: ___GIT_REVIEW___123456
Author Date: Thu, 29 Nov 2018 16:24:26 +0000
Author Email: testing@git-review.com
Committer Name: Git Review
Committer Date: Thu, 29 Nov 2018 16:24:26 +0000
Committer Email: testing@git-review.com
Name Status:
A	feature-commits-file-a.txt
EOT;

        $this->process->shouldReceive('simple')
            ->with("/usr/bin/git log -1 a6e77b8f6686a6b97cd5cf27ad1cc3f6894edc0d --pretty=format:\"%b\"")
            ->andReturn($process = Mockery::mock(GitReviewProcess::class));

        $this->processor->shouldReceive('process')->with($process)->once()->andReturn($process);
        $process->shouldReceive('getOutput')->andReturn($commitBody = "Commit One Body");

        $this->process->shouldReceive('simple')
            ->with("/usr/bin/git rev-parse --show-toplevel")
            ->andReturn($processTwo = Mockery::mock(GitReviewProcess::class));

        $this->processor->shouldReceive('process')->with(Mockery::on(function ($process) {
            return $process instanceof GitReviewProcess;
        }))->andReturn($processTwo);
        $processTwo->shouldReceive('getOutput')->andReturn("/tmp/test");

        $actual = $this->sut->parse($commit);

        $fileCollectionOne = (new FileCollection(\getcwd()));
        $fileCollectionOne->append(
            new File("A", "/tmp/test/feature-commits-file-a.txt", "/tmp/test")
        );

        $commit = new Commit(
            "a6e77b8f6686a6b97cd5cf27ad1cc3f6894edc0d",
            ["4385b4ee83c6afb85434eb9aab36f94a347738b5"],
            new Message("Commit subject a", $commitBody),
            new Author(
                "___GIT_REVIEW___123456",
                "Thu, 29 Nov 2018 16:24:26 +0000",
                "testing@git-review.com",
                "Git Review",
                "Thu, 29 Nov 2018 16:24:26 +0000",
                "testing@git-review.com"
            ),
            $fileCollectionOne
        );

        $this->assertEquals($commit, $actual);
    }
}
