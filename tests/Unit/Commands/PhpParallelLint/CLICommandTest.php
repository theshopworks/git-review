<?php

namespace Shopworks\Tests\Unit\Commands\PhpParallelLint;

use Shopworks\Git\Review\Commands\PhpParallelLint\CLICommand;
use Shopworks\Tests\UnitTestCase;

class CLICommandTest extends UnitTestCase
{
    protected function setUp(): void
    {
        parent::setUp();
    }

    /** @test */
    public function it_can_build_a_command_for_all_config_options(): void
    {
        $command = new CLICommand([
            'bin_path' => 'vendor/bin/parallel-lint',
        ], ['app/', 'tests/*/examples']);

        $this->assertEquals(
            "php vendor/bin/parallel-lint app/ tests/*/examples",
            $command->toString()
        );
    }
}
