<?php

namespace Shopworks\Tests\Unit\Commands\PhpCodeSniffer;

use Shopworks\Git\Review\Commands\PhpCodeSniffer\CLICommand;
use Shopworks\Tests\UnitTestCase;

class CLICommandTest extends UnitTestCase
{
    protected function setUp(): void
    {
        parent::setUp();
    }

    /** @test */
    public function it_can_build_a_command_for_all_config_options(): void
    {
        $command = new CLICommand([
            'bin_path' => 'vendor/bin/phpcs',
            'config_path' => '.php_cs',
            'verbosity_level' => 1,
        ], ['app/', 'tests/*/examples']);

        $this->assertEquals(
            "php vendor/bin/phpcs app/ tests/*/examples -p -v",
            $command->toString()
        );
    }

    /** @test */
    public function it_adds_the_necessary_defaults_when_no_config_is_provided_but_paths_are(): void
    {
        $command = new CLICommand([], ['app/', 'tests/*/examples']);

        $this->assertEquals("php vendor/bin/phpcs app/ tests/*/examples -p", $command->toString());
    }

    /** @test */
    public function it_can_handle_all_verbosity_levels(): void
    {
        $command = new CLICommand([
            'verbosity_level' => 1,
        ], ['app/', 'tests/*/examples']);

        $this->assertEquals("php vendor/bin/phpcs app/ tests/*/examples -p -v", $command->toString());

        $command = new CLICommand([
            'verbosity_level' => 2,
        ], ['app/', 'tests/*/examples']);

        $this->assertEquals("php vendor/bin/phpcs app/ tests/*/examples -p -vv", $command->toString());

        $command = new CLICommand([
            'verbosity_level' => 3,
        ], ['app/', 'tests/*/examples']);

        $this->assertEquals("php vendor/bin/phpcs app/ tests/*/examples -p -vvv", $command->toString());
    }
}
