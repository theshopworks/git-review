<?php

namespace Shopworks\Tests\Unit\Commands\PhpUnit;

use Illuminate\Container\Container;
use Illuminate\Support\Collection;
use Mockery;
use Mockery\MockInterface;
use Shopworks\Git\Review\Commands\PhpUnit\Command;
use Shopworks\Git\Review\File\File;
use Shopworks\Git\Review\File\GitFilesFinder;
use Shopworks\Git\Review\Process\Process as GitReviewProcess;
use Shopworks\Git\Review\Process\Processor;
use Shopworks\Git\Review\Repositories\ConfigRepository;
use Shopworks\Git\Review\VersionControl\GitBranch;
use Shopworks\Tests\UnitTestCase;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;

class CommandTest extends UnitTestCase
{
    /** @var CommandTester $commandTester */
    private $commandTester;
    /** @var Command $command */
    private $command;
    /** @var MockInterface|GitReviewProcess $process */
    private $process;
    /** @var MockInterface|Processor $processor */
    private $processor;
    /** @var MockInterface|ConfigRepository $configRepository */
    private $configRepository;
    /** @var MockInterface|GitFilesFinder $gitFilesFinder */
    private $gitFilesFinder;
    /** @var MockInterface|GitBranch $git */
    private $git;

    protected function setUp(): void
    {
        parent::setUp();

        $this->git = Mockery::mock(GitBranch::class);
        $this->process = Mockery::mock(GitReviewProcess::class);
        $this->processor = Mockery::mock(Processor::class);
        $this->gitFilesFinder = Mockery::mock(GitFilesFinder::class);
        $this->configRepository = Mockery::mock(ConfigRepository::class);

        $this->command = new Command(
            $this->process,
            $this->processor,
            $this->gitFilesFinder,
            $this->configRepository,
            $this->git
        );
        $this->commandTester = $this->getCommandTester();
    }

    /** @test */
    public function it_can_handle_when_there_are_tests_modified_or_added_on_a_topic_branch(): void
    {
        $this->gitFilesFinder->shouldReceive('getBranchName')->andReturn("example-branch");

        $this->git->shouldReceive('getChangedFiles')
            ->once()
            ->andReturn(new Collection([
                Mockery::mock(File::class, [
                    'containsPhpUnitClass' => true,
                    'getRelativePath' => 'tests/ExampleOneTest.php',
                ]),
                Mockery::mock(File::class, [
                    'containsPhpUnitClass' => true,
                    'getRelativePath' => 'tests/ExampleTwoTest.php',
                ]),
            ]));

        $this->process->shouldReceive('simple')
            ->with('php vendor/bin/phpunit tests/ExampleOneTest.php')
            ->andReturn($processOne = Mockery::mock(GitReviewProcess::class));

        $processOne->shouldReceive('getExitCode')->once()->andReturn(0);
        $processOne->shouldReceive('isSuccessful')->once()->andReturn(true);

        $this->processor->shouldReceive('process')->with($processOne, true)->andReturn($processOne);

        $this->process->shouldReceive('simple')
            ->with('php vendor/bin/phpunit tests/ExampleTwoTest.php')
            ->andReturn($processTwo = Mockery::mock(GitReviewProcess::class));

        $processTwo->shouldReceive('getExitCode')->once()->andReturn(0);
        $processTwo->shouldReceive('isSuccessful')->once()->andReturn(true);
        $this->processor->shouldReceive('process')->with($processTwo, true)->andReturn($processTwo);

        $this->commandTester->execute([
            'command' => $this->command->getName(),
        ]);

        $output = $this->commandTester->getDisplay();

        $this->assertEquals(0, $this->commandTester->getStatusCode());
        $this->assertEquals(
            \file_get_contents(__DIR__ . '/../../../fixtures/PhpUnit/topic-branch-with-tests-passing.txt'),
            $output
        );
    }

    /** @test */
    public function it_can_handle_when_there_are_tests_modified_or_added_on_a_topic_branch_and_some_of_those_tests_fail(): void
    {
        $this->gitFilesFinder->shouldReceive('getBranchName')->andReturn("example-branch");

        $this->git->shouldReceive('getChangedFiles')
            ->once()
            ->andReturn(new Collection([
                Mockery::mock(File::class, [
                    'containsPhpUnitClass' => true,
                    'getRelativePath' => 'tests/ExampleOneTest.php',
                ]),
                Mockery::mock(File::class, [
                    'containsPhpUnitClass' => true,
                    'getRelativePath' => 'tests/ExampleTwoTest.php',
                ]),
            ]));

        $this->process->shouldReceive('simple')
            ->with('php vendor/bin/phpunit tests/ExampleOneTest.php')
            ->andReturn($processOne = Mockery::mock(GitReviewProcess::class));

        $processOne->shouldReceive('getExitCode')->once()->andReturn(0);
        $processOne->shouldReceive('isSuccessful')->once()->andReturn(true);

        $this->processor->shouldReceive('process')->with($processOne, true)->andReturn($processOne);

        $this->process->shouldReceive('simple')
            ->with('php vendor/bin/phpunit tests/ExampleTwoTest.php')
            ->andReturn($processTwo = Mockery::mock(GitReviewProcess::class));

        $processTwo->shouldReceive('getExitCode')->once()->andReturn(1);
        $processTwo->shouldReceive('isSuccessful')->once()->andReturn(true);
        $this->processor->shouldReceive('process')->with($processTwo, true)->andReturn($processTwo);

        $this->commandTester->execute([
            'command' => $this->command->getName(),
        ]);

        $output = $this->commandTester->getDisplay();

        $this->assertEquals(1, $this->commandTester->getStatusCode());
        $this->assertEquals(
            \file_get_contents(__DIR__ . '/../../../fixtures/PhpUnit/topic-branch-with-tests-failing.txt'),
            $output
        );
    }

    /** @test */
    public function it_can_handle_when_there_are_no_tests_modified_or_added_on_a_topic_branch(): void
    {
        $this->gitFilesFinder->shouldReceive('getBranchName')->andReturn("example-branch");

        $this->git->shouldReceive('getChangedFiles')
            ->once()
            ->andReturn(new Collection([
                Mockery::mock(File::class, [
                    'containsPhpUnitClass' => false,
                ]),
            ]));

        $this->commandTester->execute([
            'command' => $this->command->getName(),
        ]);

        $output = $this->commandTester->getDisplay();

        $this->assertEquals(0, $this->commandTester->getStatusCode());
        $this->assertEquals(
            \file_get_contents(__DIR__ . '/../../../fixtures/PhpUnit/no-tests-found-on-topic.txt'),
            $output
        );
    }

    /** @test */
    public function it_can_tests_entire_suite_when_current_branch_is_master(): void
    {
        $this->gitFilesFinder->shouldReceive('getBranchName')->andReturn("master");

        $this->git->shouldReceive('getChangedFiles')
            ->once()
            ->andReturn(new Collection([
                Mockery::mock(File::class, [
                    'containsPhpUnitClass' => false,
                ]),
            ]));

        $this->process->shouldReceive('simple')
            ->with('php vendor/bin/phpunit')
            ->andReturn($process = Mockery::mock(GitReviewProcess::class));

        $process->shouldReceive('getExitCode')->once()->andReturn(0);
        $process->shouldReceive('isSuccessful')->once()->andReturn(true);
        $this->processor->shouldReceive('process')->with($process, true)->andReturn($process);

        $this->commandTester->execute([
            'command' => $this->command->getName(),
        ]);

        $output = $this->commandTester->getDisplay();

        $this->assertEquals(0, $this->commandTester->getStatusCode());
        $this->assertEquals(
            \file_get_contents(__DIR__ . '/../../../fixtures/PhpUnit/master-branch.txt'),
            $output
        );
    }

    /** @test */
    public function it_can_run_the_appropriate_command_for_staged_changes_only(): void
    {
        $this->gitFilesFinder->shouldReceive('getBranchName')->andReturn("example-branch");

        $this->git->shouldReceive('getStagedFiles')
            ->once()
            ->andReturn(new Collection([
                Mockery::mock(File::class, [
                    'containsPhpUnitClass' => true,
                    'getRelativePath' => 'tests/ExampleOneTest.php',
                ]),
                Mockery::mock(File::class, [
                    'containsPhpUnitClass' => true,
                    'getRelativePath' => 'tests/ExampleTwoTest.php',
                ]),
            ]));

        $this->process->shouldReceive('simple')
            ->with('php vendor/bin/phpunit tests/ExampleOneTest.php')
            ->andReturn($processOne = Mockery::mock(GitReviewProcess::class));

        $processOne->shouldReceive('getExitCode')->once()->andReturn(0);
        $processOne->shouldReceive('isSuccessful')->once()->andReturn(true);

        $this->processor->shouldReceive('process')->with($processOne, true)->andReturn($processOne);

        $this->process->shouldReceive('simple')
            ->with('php vendor/bin/phpunit tests/ExampleTwoTest.php')
            ->andReturn($processTwo = Mockery::mock(GitReviewProcess::class));

        $processTwo->shouldReceive('getExitCode')->once()->andReturn(0);
        $processTwo->shouldReceive('isSuccessful')->once()->andReturn(true);
        $this->processor->shouldReceive('process')->with($processTwo, true)->andReturn($processTwo);

        $this->commandTester->execute([
            'command' => $this->command->getName(),
            '--staged-only' => true,
        ]);

        $output = $this->commandTester->getDisplay();

        $this->assertEquals(0, $this->commandTester->getStatusCode());
        $this->assertEquals(
            \file_get_contents(__DIR__ . '/../../../fixtures/PhpUnit/topic-branch-with-tests-passing.txt'),
            $output
        );
    }

    /** @test */
    public function it_can_show_a_list_of_tests_files_as_part_of_branch(): void
    {
        $this->gitFilesFinder->shouldReceive('getBranchName')->andReturn("example-branch");

        $this->git->shouldReceive('getChangedFiles')
            ->once()
            ->andReturn(new Collection([
                Mockery::mock(File::class, [
                    'containsPhpUnitClass' => true,
                    'getRelativePath' => 'tests/ExampleOneTest.php',
                ]),
                Mockery::mock(File::class, [
                    'containsPhpUnitClass' => true,
                    'getRelativePath' => 'tests/ExampleTwoTest.php',
                ]),
            ]));

        $this->process->shouldNotReceive('simple');

        $this->commandTester->execute([
            'command' => $this->command->getName(),
            '--list-files' => true,
        ]);

        $output = $this->commandTester->getDisplay();

        $this->assertEquals(0, $this->commandTester->getStatusCode());

        $expectedOutput = <<<'EOT'
tests/ExampleOneTest.php
tests/ExampleTwoTest.php

EOT;

        $this->assertEquals($expectedOutput, $output);
    }

    /** @test */
    public function it_can_show_a_list_of_tests_files_as_part_of_branch_when_also_specifying_staged_only_option(): void
    {
        $this->gitFilesFinder->shouldReceive('getBranchName')->andReturn("example-branch");

        $this->git->shouldReceive('getStagedFiles')
            ->once()
            ->andReturn(new Collection([
                Mockery::mock(File::class, [
                    'containsPhpUnitClass' => true,
                    'getRelativePath' => 'tests/ExampleOneTest.php',
                ]),
                Mockery::mock(File::class, [
                    'containsPhpUnitClass' => true,
                    'getRelativePath' => 'tests/ExampleTwoTest.php',
                ]),
            ]));

        $this->process->shouldNotReceive('simple');

        $this->commandTester->execute([
            'command' => $this->command->getName(),
            '--list-files' => true,
            '--staged-only' => true,
        ]);

        $output = $this->commandTester->getDisplay();

        $this->assertEquals(0, $this->commandTester->getStatusCode());

        $expectedOutput = <<<'EOT'
tests/ExampleOneTest.php
tests/ExampleTwoTest.php

EOT;

        $this->assertEquals($expectedOutput, $output);
    }

    private function getCommandTester(): CommandTester
    {
        $consoleApplication = new Application();

        $this->command->setLaravel(new Container());

        $consoleApplication->add($this->command);
        $command = $consoleApplication->find('phpunit');

        return new CommandTester($command);
    }
}
