
Filtering changed files on branch using the following paths:
============================================================

 * app/
 * fruits/*/tests

Modified files on branch "pineapples"

app/example1.php - added
fruits/oranges/tests/example3.php - added
fruits/pineapples/tests/example2.php - modified

Running command:

php vendor/bin/phpstan analyse app/example1.php fruits/oranges/tests/example3.php fruits/pineapples/tests/example2.php -c phpstan.neon -l 7


 [OK] PHPStan checks passed, good job!!!!                                       

