<?php

namespace Shopworks\Git\Review\VersionControl;

class DiffStatus
{
    private $status;
    private $relativePath;
    private $relativePathBeforeRename;

    public function __construct(
        string $status,
        string $relativePath,
        ?string $relativePathBeforeRename = null
    ) {
        $this->status = $status;
        $this->relativePath = $relativePath;
        $this->relativePathBeforeRename = $relativePathBeforeRename;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function getRelativePath(): string
    {
        return $this->relativePath;
    }

    public function getRelativePathBeforeRename(): ?string
    {
        return $this->relativePathBeforeRename;
    }
}
