<?php

declare(strict_types=1);

namespace Shopworks\Git\Review\Commands\PhpCodeSniffer;

use Shopworks\Git\Review\BaseCommand;
use Shopworks\Git\Review\Commands\CliCommandContract;

class Command extends BaseCommand
{
    protected $signature = 'phpcs';
    protected $toolName = 'PHP Code Sniffer';
    protected $config = 'tools.php_cs';
    protected $configPaths = 'tools.php_cs.paths';

    protected function resolveCommand(array $ymlConfig, array $paths): CliCommandContract
    {
        return new CLICommand($ymlConfig, $paths);
    }
}
