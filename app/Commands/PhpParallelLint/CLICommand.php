<?php

declare(strict_types=1);

namespace Shopworks\Git\Review\Commands\PhpParallelLint;

use Illuminate\Support\Arr;
use Shopworks\Git\Review\Commands\CliCommandContract;

class CLICommand implements CliCommandContract
{
    private $config;
    private $filePaths;

    public function __construct(array $config, array $filePaths)
    {
        $this->config = $config;
        $this->filePaths = $filePaths;
    }

    public function toString(): string
    {
        $binPath = Arr::get($this->config, 'bin_path', 'vendor/bin/parallel-lint');

        $additionalOptions = $this->addFilePaths();

        return "php {$binPath} " . $additionalOptions;
    }

    private function addFilePaths(): string
    {
        return \implode(' ', $this->filePaths);
    }
}
