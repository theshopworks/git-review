<?php

declare(strict_types=1);

namespace Shopworks\Git\Review\Commands\PhpParallelLint;

use Shopworks\Git\Review\BaseCommand;
use Shopworks\Git\Review\Commands\CliCommandContract;

class Command extends BaseCommand
{
    protected $signature = 'parallel-lint';
    protected $toolName = 'PHP Parallel Lint';
    protected $config = 'tools.php_parallel_lint';
    protected $configPaths = 'tools.php_parallel_lint.paths';

    protected function resolveCommand(array $ymlConfig, array $paths): CliCommandContract
    {
        return new CLICommand($ymlConfig, $paths);
    }
}
