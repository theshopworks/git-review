<?php

declare(strict_types=1);

namespace Shopworks\Git\Review\Commands\PhpCodeBeautifier;

use Shopworks\Git\Review\BaseCommand;
use Shopworks\Git\Review\Commands\CliCommandContract;

class Command extends BaseCommand
{
    protected $signature = 'phpcbf';
    protected $toolName = 'PHP Code Beautifier';
    protected $config = 'tools.php_cbf';
    protected $configPaths = 'tools.php_cbf.paths';

    protected function resolveCommand(array $ymlConfig, array $paths): CliCommandContract
    {
        return new CLICommand($ymlConfig, $paths);
    }

    protected function getErrorMessage(): string
    {
        return 'PHP Code Beautifier fixed some issues.';
    }

    protected function getSuccessMessage(): string
    {
        return 'PHP Code Beautifier had nothing to fix, good job!!!!';
    }
}
